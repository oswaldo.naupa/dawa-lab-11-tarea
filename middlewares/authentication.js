const jwt = require('jsonwebtoken');


let verificaToken = (req, res, next) => {
    let token = req.cookies.token;
    //let token = req.get('token');
    //let token = localStorage.getItem("token")
    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        if(err) {
            /*return res.status(401).json({
                ok: false,
                err,
            });*/
            //let error = "Necesita iniciar sesion para continuar"
            res.redirect('/login');      
        }
        req.user = decoded.usuario;
        console.log(req.user)
        next();
    });
};


module.exports = {
    verificaToken,
};