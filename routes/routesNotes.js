const express = require('express');
const Nota = require('../models/inventario');
const Noticias = require('../models/noticias');
const Usuario = require('../models/usuario');
const router = express.Router();

router.get('/notas', async function(req, res){
    let error = [];
    const notas = await Nota.find({});
    const usersName = await Usuario.find({});
    res.render('tareas', {error, notas, usersName })
});
router.get("/blog", async function (req, res) {
    const noticias = await Noticias.find({});
    res.render('blog', { noticias })
});
router.post('/blog_crear',  async function(req, res) {
    const { titulo, encabezado, descripcion, image, autor, categoria, tag, tag1, tag2 } = req.body;
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    let newNota = new Noticias({ titulo, encabezado, descripcion, image, autor, categoria, tags });
    await newNota.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/blog");
});
router.get("/blog/:id", async function (req, res) {
    const noticia = await Noticias.findById(req.params.id);
    res.render('blog-single', {noticia: noticia})
});
router.post('/blog/:id/comentar',  async function(req, res) {
    const { nombre_usuario, img_usuario, comentario } = req.body;
    const noticia = await Noticias.findById(req.params.id);
    noticia.comentarios.push({ nombre_usuario, img_usuario, comentario });
    await noticia.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/blog/" + req.params.id);
});
router.get('/editNote/:id', async function(req, res){
    let error = [];
    const usersName = await Usuario.find({});
    const nota = await Nota.findById(req.params.id);
    res.render('editNote', {error, nota, usersName })
});

router.post('/newNota',  async function(req, res) {
    let error = [];
    const { titulo, descripcion, user } = req.body;
    const errors = [];
    if (!titulo) {
        errors.push({ text: "Título es requerido." });
    }
    if (!descripcion) {
        errors.push({ text: "Descripción es requerido." });
    }
    if (errors.length > 0) {
        res.render("tareas", {
            error,
        });
    } else {
        let newNota = new Nota({ title: titulo, description: descripcion, user: user, });
        await newNota.save();
        res.redirect("/notas");
    }
});

router.put('/updateNota/:id', async function(req, res){
    const { titulo,  descripcion, user } = req.body;
    await Nota.findByIdAndUpdate(req.params.id, { title: titulo, description: descripcion, user: user });
    res.redirect("/notas");
})

router.delete('/deleteNota/:id', async function(req, res) {
    let id = req.params.id;
    await Nota.findByIdAndDelete(id);
    res.redirect("/notas");
});
module.exports = router