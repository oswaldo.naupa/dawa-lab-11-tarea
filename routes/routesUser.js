const express = require('express');
const Usuario = require('../models/usuario');
const router = express.Router();
const jwt = require("jsonwebtoken")

//google
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);


router.get('/login', function(req, res){
    let error = [];
    //error.push({ text: res.errortoken});
    res.render('login', {error,
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});

router.get('/register', function(req, res){
    let error = [];
    res.render('register', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/welcome', function(req, res){
    res.render('welcome')
});
router.post('/registrar',  async function(req, res) {
    let error = [];
    const { username, email, password, confirmPassword } = req.body;
    if (password != confirmPassword) {
        error.push({ text: "Contraseñas no coinciden." });
        res.render("register", {
            error,
            username,
            email,
            password,
            confirmPassword
        });
    } else {
        // Look for email coincidence
        const emailUser = await Usuario.findOne({ email: email });
        const nameUser = await Usuario.findOne({ nombre: username });
        if (emailUser) {
            error.push({ text: "El correo ya esta en uso." });
        }
        if (nameUser) {
            error.push({ text: "El usuario ya esta en uso." });
        }
        if(error.length >0 ){
            res.render("register", {
                error,
                username,
                email,
                password,
                confirmPassword
            });
        } else {
          // Saving a New User
          const newUser = new Usuario({ nombre:username, email:email, password:password });
          newUser.password = await newUser.encryptPassword(password);
          await newUser.save();
          res.redirect("/welcome");
        }
    }
    
});
router.post('/ingresar', async function(req, res) {
    let error = [];
    const { email, password } = req.body;
    const user = await Usuario.findOne({email: email});
    if (!user) {
        error.push({ text: "Usuario invalido." });
        res.render("login", {
            error,
            email,
            password
        });
    } else {
        const match = await user.matchPassword(password);
        if(match) {
            let token = jwt.sign({
                usuario: user
            }, process.env.SEED,{expiresIn: process.env.CADUCIDAD_TOKEN})
            res.cookie('token', token)
            //localStorage.getItem('token', token)
            //console.log(token)
            res.redirect('/')
            
        } else {
            error.push({ text: "Contraseña invalida." });
            res.render("login", {
                error,
                email,
                password
            });
        }
        
    }
});
router.get('/salir', (req, res) => {
    res.clearCookie("token");
    localStorage.removeItem('myFirstKey');
    localStorage.removeItem('name');
    //res.redirect("https://mail.google.com/mail/u/0/?logout&hl=en");
    res.redirect("/")
  });

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();
    
    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);

    return payload;
    
  }

router.post("/google", async (req, res) => {
    let token = req.body.idtoken;

    let googleUser = await verify(token).catch((e) => {
        return res.status(403).json({
            ok: false,
            err: e,
        });
    });
    

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Debe de usar su autenticación normal",
                    },
                });
            } else {
                let token = jwt.sign(
                    {
                      usuario: usuarioDB,
                    },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );
                res.cookie('token', token);
                localStorage.setItem('myFirstKey', googleUser.picture);
                localStorage.setItem('name', googleUser.name);
                //localStorage.getItem('token', token)
                //console.log(token)
               return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            }
        } else {
            // Si usuario no existe en nuestra base de datos

            let usuario = new Usuario();

            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.picture;
            usuario.google = true;
            usuario.password = "123";

            usuario.save((err, usuarioDB) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err,
                    });
                }

                let token = jwt.sign(
                    {
                      usuario: usuarioDB,
                    },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );
                res.cookie('token', token)
                localStorage.setItem('myFirstKey', googleUser.picture);
                localStorage.setItem('name', googleUser.name);
                //localStorage.getItem('token', token)
                //console.log(token)
               // res.redirect('/')

                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            });
        }
    });
});

module.exports = router