const express = require('express');
const Peliculas = require('../models/peliculas');
const Usuario = require('../models/usuario');
const app = express();
const {verificaToken} = require("../middlewares/authentication")

const fileUpload = require("express-fileupload");
//default options
app.use(fileUpload({ useTempFiles: true}));

//borrar archivo
const fs = require('fs')
const path = require('path')

if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }


app.get("/pelicula", async function (req, res) {
    const peliculas = await Peliculas.find({});
    res.render('pelicula', { peliculas })
});
app.get("/pelicula/categoria/:categoria", async function (req, res) {
    const categoria = req.params.categoria
    const peliculas = await Peliculas.find({categoria:req.params.categoria});
    res.render('pelicula', {peliculas: peliculas, categoria:categoria})
});
app.get('/agregarPelicula/',verificaToken, async function(req, res){
    let error = [];
    let titulo = "";
    let descripcion = "";
    let categoria ="";
    let tag="";
    let tag1="";
    let tag2="";
    let errorextension="";
    res.render('agregarPelicula', {error,titulo,descripcion,categoria,tag2,tag1,tag},errorextension)
});
app.post('/pelicula_crear',verificaToken, async function(req, res) {
    const { titulo, descripcion, categoria, tag, tag1, tag2 } = req.body;
    let tags = [];
    tags.push(tag);

    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    //console.log(titulo)
    //console.log(req.files)

    if (!(req.files)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }
   

    //extension
    let archivo = req.files.image;

    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    
    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];

    if (extensionesValidas.indexOf(extension) < 0) {
        let error = [];
        let errorextension ="Formato de archivo no valido"
        return res.render("agregarPelicula",{error,titulo,descripcion,categoria,tag2,tag1,tag, errorextension});
    }
    //Nombre archivo
    let nombreArchivo = `${titulo.replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase()}-${new Date().getMilliseconds()}.${extension}`;
    await archivo.mv(`public/assets/uploads/peliculas/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

    });

     let newPelicula = new Peliculas({ titulo, descripcion, image: nombreArchivo, categoria, tags });
     await newPelicula.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/pelicula");
});
app.get("/pelicula/:id", async function (req, res) {
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('pelicula-single', {pelicula: pelicula})
});
app.post('/pelicula/:id/comentar',  async function(req, res) {
    const { nombre_usuario, comentario } = req.body;
    const noticia = await Peliculas.findById(req.params.id);
    let img_usuario = 'usuario.png';
    noticia.comentarios.push({ nombre_usuario, img_usuario, comentario });
    await noticia.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/pelicula/" + req.params.id);
});


app.get('/editPelicula/:id',verificaToken, async function(req, res){
    let error = [];
    let errorextension = ""
    const usersName = await Usuario.find({});
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('editPelicula', {error, pelicula, usersName, errorextension, id:req.params.d })
});
app.put('/updatepelicula/:id',verificaToken, async function(req, res){
    const {id2, titulo,descripcion,imageAnterior,categoria,tag,tag1,tag2 } = req.body;
    let id= req.params.id
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)

    if (!(req.files)) {
        if(id2 === 'undefined'){
            await Peliculas.findByIdAndUpdate(id, { titulo: titulo,descripcion: descripcion, image: imageAnterior,categoria:categoria,tags:tags });
        }else {
            await Peliculas.findByIdAndUpdate(id2, { titulo: titulo,descripcion: descripcion, image: imageAnterior,categoria:categoria,tags:tags });    
        }
        return res.redirect("/pelicula");
    }

    //extension
    let archivo = req.files.image;

    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];

    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];

    if (extensionesValidas.indexOf(extension) < 0) {
        let error = [];
        let errorextension = "Formato de archivo no valido"
        const usersName = await Usuario.find({});
        const pelicula = Peliculas({titulo, descripcion, image: imageAnterior, categoria, tags });
        return res.render('editPelicula', {error, pelicula, usersName, errorextension,id })
    }
    //Nombre archivo
    let nombreArchivo = `${titulo.replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase()}-${new Date().getMilliseconds()}.${extension}`;
    await archivo.mv(`public/assets/uploads/peliculas/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        if(id2 === 'undefined'){
            imagenUsuario(id,imageAnterior,titulo,descripcion,categoria,tags, res, nombreArchivo);
        }else {
            imagenUsuario(id2,imageAnterior,titulo,descripcion,categoria,tags, res, nombreArchivo);        }
    });

    //await Peliculas.findByIdAndUpdate(req.params.id, { titulo: titulo,descripcion: descripcion, image: nombreArchivo,categoria:categoria,tags:tags });
    res.redirect("/pelicula");
})
app.delete('/deletePelicula/:id',verificaToken, async function(req, res) {
    let id = req.params.id;
    const peliculas = await Peliculas.findById(id);
    const image = peliculas.image
    console.log(peliculas.image)
    let pathImagen = path.resolve(__dirname, `../public/assets/uploads/peliculas/${image}`)
        console.log(pathImagen)
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }
    await Peliculas.findByIdAndDelete(id);
    
    res.redirect("/pelicula");
    
});

function imagenUsuario(id,imageAnterior,titulo,descripcion,categoria,tags, res, nombreArchivo) {
    Peliculas.findById(id, (err, usuarioDB) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

        if (!usuarioDB) {
            return res.status(400).json({
                ok:false,
                err: {
                    message :
                    "Usuario no existe",
                }, 
            });
        }

        let pathImagen = path.resolve(__dirname, `../public/assets/uploads/peliculas/${imageAnterior}`)
        console.log(pathImagen)
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }

        /*let pathImagen = path.resolve(__dirname, `../public/assets/uploads/usuarios/${usuarioDB.img}`)
        console.log(pathImagen)
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }*/

        usuarioDB.image = nombreArchivo;
        usuarioDB.titulo= titulo
        usuarioDB.descripcion = descripcion
        usuarioDB.categoria = categoria
        usuarioDB.tags = tags

        usuarioDB.save((err, usuarioGuardado) => {
        });
    });
}



module.exports = app